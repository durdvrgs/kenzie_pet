from django.db import models

class Characteristic(models.Model):
    
    characteristic = models.CharField(max_length=255, unique=True)

class Group(models.Model):
    
    name = models.CharField(max_length=255, unique=True)
    scientific_name = models.CharField(max_length=255, unique=True)

class Animal(models.Model):
    
    name = models.CharField(max_length=255)
    age =  models.FloatField()
    weight = models.FloatField()
    sex =  models.CharField(max_length=255)
    
    group = models.ForeignKey(Group, on_delete=models.SET_NULL, blank=True, null=True)
    characteristic_set = models.ManyToManyField(Characteristic)