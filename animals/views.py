from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import Animal
from .serializers import AnimalSerializer, GroupSerializer
from .services import AnimalServices

class AnimalView(APIView):
    
    def get(self, request, animal_id=None):
        
        if not animal_id:
            
            all_pets = AnimalServices.get_all_pets()
            serializer = AnimalSerializer(all_pets, many=True)
            
            return Response(serializer.data, status=status.HTTP_200_OK)
        
        else:
    
            pet = AnimalServices.get_pet_for_id(animal_id)
            
            if pet == 404:
                
                return Response({'message': 'not found'}, status=status.HTTP_404_NOT_FOUND)
            
            serializer = AnimalSerializer(pet)
            
            return Response(serializer.data, status=status.HTTP_200_OK)
    
    def post(self, request):
        
        serializer = AnimalSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        pet_data = request.data
        group = pet_data.pop('group')
        characteristic_set = pet_data.pop('characteristic_set')
        
        this_pet_group = AnimalServices.get_or_create_pet_group(group)
        
        if this_pet_group == 400:
            return Response({'msg': 'invalid name or scientific name group'}, status=status.HTTP_400_BAD_REQUEST)
        
        new_pet = AnimalServices.create_new_pet(pet_data, this_pet_group)
        AnimalServices.add_characteristic_to_pet(characteristic_set, new_pet)
            
        serializer = AnimalSerializer(new_pet)

        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    def delete(self, request, animal_id):
        
        pet = AnimalServices.delete_pet(animal_id)
        
        if pet == 404:
            
            return Response({'msg': 'not found'}, status=status.HTTP_404_NOT_FOUND)
        
        return Response(status=status.HTTP_204_NO_CONTENT)
