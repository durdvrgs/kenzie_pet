# Generated by Django 3.2 on 2021-05-04 17:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('animals', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='characteristic',
            name='characteristic',
            field=models.CharField(max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='group',
            name='name',
            field=models.CharField(max_length=255, unique=True),
        ),
        migrations.AlterField(
            model_name='group',
            name='scientific_name',
            field=models.CharField(max_length=255, unique=True),
        ),
    ]
