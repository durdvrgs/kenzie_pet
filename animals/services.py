from .models import Group, Characteristic, Animal

class AnimalServices():
    
    @staticmethod
    def get_all_pets():
        
        return Animal.objects.all()
    
    @staticmethod
    def get_pet_for_id(pet_id):
        
        try:
            
            return Animal.objects.get(id=pet_id)
        except:
            
            return 404
    
    @staticmethod
    def create_new_pet(pet_data, this_pet_group):
        
        return Animal.objects.create(**pet_data, group=this_pet_group)
        
    @staticmethod
    def get_or_create_pet_group(group):
        
        try:
            
            this_pet_group = Group.objects.get_or_create(**group)
            return this_pet_group[0]
        
        except:
            
            return 400
             
    @staticmethod
    def add_characteristic_to_pet(characteristic_set, pet):
        
        for characteristic in characteristic_set:
            
            this_pet_characteristic = characteristic['characteristic']
            
            try:
                
                characteristic = Characteristic.objects.get(characteristic=this_pet_characteristic)
                pet.characteristic_set.add(characteristic)
                
            except:
                
                characteristic = Characteristic.objects.create(characteristic=this_pet_characteristic)
                pet.characteristic_set.add(characteristic)
    
    @staticmethod
    def delete_pet(pet_id):
        
        try:
            
            pet = Animal.objects.get(id=pet_id)
            pet.delete()
        
        except:
            
            return 404
        