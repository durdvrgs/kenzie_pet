# kenzie_pet API

API para cadastro de Pets, com as seguintes Models:

- Animal (podendo cadastrar name, weight, age, sex, caracteristics e group)

- Group (1:N com Animal, recebe name, por ex:'cão'; e scientific_name, por ex: 'canis familiares')

- Characteristic (N:N com Animal, recebe uma lista de characteristic, por ex: 'peludo', 'médio porte' ... )

## Instalação

- python3 -m venv venv
- pip install django djangorestframework

## Utilização

### Routes GET method:

"api/animals/" - retorna lista com todos animais cadastrados

```
HTTP 200
[
  {
    "id": 1,
    "name": "Bidu",
    "age": 1.0,
    "weight": 30.0,
    "sex": "macho",
    "group": {
      "id": 1,
      "name": "cao",
      "scientific_name": "canis familiaris"
    },
    "characteristic_set": [
      {
        "id": 1,
        "characteristic": "peludo"
      },
      {
        "id": 2,
        "characteristic": "medio porte"
      }
    ]
  },
  {
    "id": 2,
    "name": "Hanna",
    "age": 1.0,
    "weight": 20.0, 
    "sex": "femea",
    "group": {
      "id": 2,
      "name": "gato",
      "scientific_name": "felis catus"
    },
    "characteristic_set": [
      {
        "id": 1,
        "characteristic": "peludo"
      },
      {
        "id": 3,
        "characteristic": "felino"
      }
    ]
  }
]
```

"api/animals/<int:animal_id>/" - Retorna um animal por ID

retorna HTTP 200:

```
  {
  "id": 1,
  "name": "Bidu",
  "age": 1.0,
  "weight": 30.0,
  "sex": "macho",
  "group": {
    "id": 1,
    "name": "cao",
    "scientific_name": "canis familiaris"
  },
  "characteristic_set": [
    {
      "id": 1,
      "characteristic": "peludo"
    },
    {
      "id": 2,
      "characteristic": "medio porte"
    }
  ]
}
```

ou 

HTTP 404

```
{
    'message': 'not found'
}
```

### Routes POST method:

"api/animals/" - espera o seguinte JSON no body da requisição:

```
{
  "name": "Bidu",
	"age": 1,
	"weight": 30,
	"sex": "macho",
  "group": {
	"name": "cao",
	"scientific_name": "canis familiaris"
	},
  "characteristic_set": [
    {
	"characteristic": "peludo"
    },
    {
	"characteristic": "medio porte"
    }
  ]
}
```

retornando com HTTP 201:

```
{
  "id": 1,
  "name": "Bidu",
  "age": 1.0,
  "weight": 30.0,
  "sex": "macho",
  "group": {
    "id": 1,
    "name": "cao",
    "scientific_name": "canis familiaris"
  },
  "characteristic_set": [
    {
      "id": 1,
      "characteristic": "peludo"
    },
    {
      "id": 2,
      "characteristic": "medio porte"
    }
  ]
}
```

### Route DELETE method:

"api/animals/<int:animal_id>/" - deleta um pet por seu ID

retorna HTTP 204 (no content) - em caso de deletado com sucesso

ou

retorna HTTP 404 (NOT_FOUND) - em caso de animal não encontrado

```
{
    'message': 'not found'
}
```
